package enums;

public enum Colors {
	AQUA,
	CYAN,
	LIME,
	YELLOW,
	HOTPINK,
	FUCHSIA,
	RED,
	MEDIUMSPRINGGREEN
}
