package Factory_AbstracFactory;

import ObjBonus.Diamant;
import ObjBonus.Hamburguesa;
import ObjBonus.Redbull;
import enums.TEnemic;
import enums.TObjBonus;
import enums.TObjCaza;
import enums.TObjMejora;
import enums.TVehicles;
import interficies.JugadorDecorator;
import src.JugadorAbst;
import src.ObjBonus;

public class ObjBonusFactory implements FactoriaAbstracta<ObjBonus>{


	@Override
	public ObjBonus create(TObjBonus t) {
		switch (t) {
		case Diamant:
			return new Diamant(5);
		case Hamburguesa:
			return new Hamburguesa(1);
		case RedBull:
			return new Redbull(2);

		default:
			return null;
		}
	}

	@Override
	public ObjBonus create(TEnemic t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjBonus create(TObjCaza t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjBonus create(TObjMejora t, JugadorDecorator a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjBonus create(TVehicles t, JugadorDecorator a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjBonus create(TObjMejora t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjBonus create(TVehicles t) {
		// TODO Auto-generated method stub
		return null;
	}


	
}
