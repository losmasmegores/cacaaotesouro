package Factory_AbstracFactory;

import ObjCaza.Cofre;
import ObjCaza.Lingot;
import ObjCaza.Moneda;
import enums.TEnemic;
import enums.TObjBonus;
import enums.TObjCaza;
import enums.TObjMejora;
import enums.TVehicles;
import interficies.JugadorDecorator;
import src.JugadorAbst;
import src.ObjCaza;

public class ObjCazaFactory implements FactoriaAbstracta<ObjCaza>{

	@Override
	public ObjCaza create(TEnemic t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjCaza create(TObjCaza t) {
		switch (t) {
		case Cofre:
			return new Cofre(50);
		case Lingot:
			return new Lingot(25);
		case Moneda:
			return new Moneda(10);

		default:
			return null;
		}
	}

	@Override
	public ObjCaza create(TObjMejora t, JugadorDecorator j) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjCaza create(TObjBonus t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjCaza create(TVehicles t, JugadorDecorator j) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjCaza create(TObjMejora t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjCaza create(TVehicles t) {
		// TODO Auto-generated method stub
		return null;
	}


}
