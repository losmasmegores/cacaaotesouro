package Factory_AbstracFactory;

import Vehicles.AlaDelta;
import Vehicles.Barca;
import Vehicles.Cotxe;
import Vehicles.Patinet;
import enums.TEnemic;
import enums.TObjBonus;
import enums.TObjCaza;
import enums.TObjMejora;
import enums.TVehicles;
import interficies.JugadorDecorator;
import src.Vehicles;

public class VehiclesFactory implements FactoriaAbstracta<Vehicles>{

	
	@Override
	public Vehicles create(TVehicles t) {
		switch (t) {
		case Ala_Delta:
			return new AlaDelta();
		case Barca:
			return new Barca();
		case Cotxe:
			return new Cotxe();
		case Patinet:
			return new Patinet();

		default:
			return null;
		}
	}
	@Override
	public Vehicles create(TVehicles t,JugadorDecorator a) {
		switch (t) {
		case Ala_Delta:
			return new AlaDelta(a);
		case Barca:
			return new Barca(a);
		case Cotxe:
			return new Cotxe(a);
		case Patinet:
			return new Patinet(a);

		default:
			return null;
		}
	}

	@Override
	public Vehicles create(TEnemic t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vehicles create(TObjCaza t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vehicles create(TObjMejora t, JugadorDecorator j) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vehicles create(TObjBonus t) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vehicles create(TObjMejora t) {
		// TODO Auto-generated method stub
		return null;
	}

}
