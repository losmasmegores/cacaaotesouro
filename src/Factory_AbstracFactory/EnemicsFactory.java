package Factory_AbstracFactory;

import Enemics.Bestia;
import Enemics.Monstre;
import Enemics.Pirata;
import enums.TEnemic;
import enums.TObjBonus;
import enums.TObjCaza;
import enums.TObjMejora;
import enums.TVehicles;
import interficies.JugadorDecorator;
import src.Enemics;
import src.JugadorAbst;

public class EnemicsFactory implements FactoriaAbstracta<Enemics>{

	@Override
	public Enemics create(TEnemic t) {
		switch (t) {
		case Bestia:
			return new Bestia(-7);
		case Pirata:
			return new Pirata(-10);
		case Monstre:
			return new Monstre(-15);

		default:
			return null;
		}
	}

	@Override
	public Enemics create(TObjCaza t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enemics create(TObjMejora t, JugadorDecorator a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enemics create(TObjBonus t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enemics create(TVehicles t, JugadorDecorator a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enemics create(TObjMejora t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enemics create(TVehicles t) {
		// TODO Auto-generated method stub
		return null;
	}


}
