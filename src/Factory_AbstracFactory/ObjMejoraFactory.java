package Factory_AbstracFactory;

import ObjMejora.Espasa;
import ObjMejora.Laxant;
import ObjMejora.Pistola;
import enums.TEnemic;
import enums.TObjBonus;
import enums.TObjCaza;
import enums.TObjMejora;
import enums.TVehicles;
import interficies.JugadorDecorator;
import src.ObjMejora;

public class ObjMejoraFactory implements FactoriaAbstracta<ObjMejora>{

	@Override
	public ObjMejora create(TEnemic t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjMejora create(TObjCaza t) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public ObjMejora create(TObjMejora t) {
		switch (t) {
		case Espasa:
			return new Espasa(0.5);
		case Laxant:
			return new Laxant(2);
		case Pistola:
			return new Pistola(0.25);

		default:
			return null;
		}
	}

	public ObjMejora create(TObjMejora t, JugadorDecorator a) {
		switch (t) {
		case Espasa:
			return new Espasa(0.5,a);
		case Laxant:
			return new Laxant(2,a);
		case Pistola:
			return new Pistola(0.25,a);

		default:
			return null;
		}
	}

	@Override
	public ObjMejora create(TObjBonus t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjMejora create(TVehicles t, JugadorDecorator j) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ObjMejora create(TVehicles t) {
		// TODO Auto-generated method stub
		return null;
	}


}
