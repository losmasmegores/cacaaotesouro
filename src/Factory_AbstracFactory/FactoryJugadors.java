package Factory_AbstracFactory;

import Jugadors.Comerciant;
import Jugadors.Guerrer;
import Jugadors.Pistoler;
import enums.Colors;
import enums.TPlayer;
import src.Jugador;

public class FactoryJugadors {

	public static Jugador getPlayer(TPlayer tipus, String nom, int saldo,Colors color) {
		Jugador player = null;
		switch (tipus) {
		case Comerciant:
			player = new Comerciant(nom, saldo,color);
			break;
		case Guerrer:
			player = new Guerrer(nom, saldo,color);
			break;
		case Pistoler:
			player = new Pistoler(nom, saldo,color);
			break;
			
		default:
			break;
		}
		return player;
	}
}
