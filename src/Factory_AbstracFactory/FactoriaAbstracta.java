package Factory_AbstracFactory;

import enums.TEnemic;
import enums.TObjBonus;
import enums.TObjCaza;
import enums.TObjMejora;
import enums.TVehicles;
import interficies.JugadorDecorator;

public interface FactoriaAbstracta<T> {
		T create(TEnemic t);
		T create(TObjCaza t);
		T create(TObjMejora t);
		T create(TObjMejora t,JugadorDecorator a);
		T create(TObjBonus t);
		T create(TVehicles t);
		T create(TVehicles t,JugadorDecorator a);
}
