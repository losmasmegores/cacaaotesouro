package Factory_AbstracFactory;

import enums.TFactoria;
import src.Enemics;
import src.ObjBonus;
import src.ObjCaza;
import src.ObjMejora;
import src.Vehicles;

public class FactoriaObjectes {
	public static FactoriaAbstracta getFactory(TFactoria a) {
		switch (a) {
		case Enemics:
			return new EnemicsFactory();
		case ObjBonus:
			return new ObjBonusFactory();
		case ObjCaza:
			return new ObjCazaFactory();
		case ObjMejora:
			return new ObjMejoraFactory();
		case Vehicle:
			return new VehiclesFactory();

		default:
			return null;
		}
	}
}