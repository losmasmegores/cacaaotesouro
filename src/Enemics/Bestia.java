package Enemics;

import enums.TEnemic;
import src.Enemics;

public class Bestia extends Enemics{

	public Bestia(String nom, int punts) {
		super(nom, TEnemic.Bestia, punts);
	}
	public Bestia(int punts) {
		super(TEnemic.Bestia, punts);
	}

}
