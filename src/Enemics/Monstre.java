package Enemics;

import enums.TEnemic;
import src.Enemics;

public class Monstre extends Enemics{
	public Monstre(String nom, int punts) {
		super(nom, TEnemic.Monstre, punts);
	}
	public Monstre(int punts) {
		super(TEnemic.Monstre, punts);
	}

}
