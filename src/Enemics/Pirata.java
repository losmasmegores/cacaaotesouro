package Enemics;

import enums.TEnemic;
import src.Enemics;

public class Pirata extends Enemics{
	public Pirata(String nom, int punts) {
		super(nom, TEnemic.Pirata, punts);
	}
	public Pirata(int punts) {
		super(TEnemic.Pirata, punts);
	}
}
