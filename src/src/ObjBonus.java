package src;

import enums.TObjBonus;
import interficies.ObjecteAGuanyar;

public class ObjBonus implements ObjecteAGuanyar{
	String nom;
	TObjBonus tipus;
	int punts_a_sumar;
	
	public ObjBonus(TObjBonus tipus, int punts) {
		this.nom = tipus.toString();
		this.tipus = tipus;
		this.punts_a_sumar = punts;
	}
	public ObjBonus(String nom, TObjBonus tipus, int punts) {
		this.nom = nom;
		this.tipus = tipus;
		this.punts_a_sumar = punts;
	}
	
	@Override
	public String toString() {
		return "ObjBonus [nom=" + nom + ", tipus=" + tipus + ", punts_a_sumar=" + punts_a_sumar + "]";
	}
	@Override
	public int getpunts() {
		return 0+this.punts_a_sumar;
	}
}
