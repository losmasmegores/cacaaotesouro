package src;

import Adapter.PuntuacionAdapter;
import Observer.Observer;

public class TipusJugador implements Observer {
	Jugador j;
//	EnemigoAdapter eAdapter;
//	BonusAdapter bAdapter;
//	CazaAdapter cAdapter;

	public TipusJugador(Jugador jo) {
		this.j = jo;
	}

	@Override
	public void actualitzaPunts(ObjecteGuanyat o, PuntuacionAdapter Adapter) {
		double puntos = Adapter.adaptarPuntuacion(o.getPunts(),j.factor);
		if (this.j.equals(o.getJugador()))
//			this.j.punts += Adapter.adaptarPuntuacion(o.getPunts(),j.factor);
			this.j.punts += puntos;
		
		if ((Object) o instanceof Enemics) {
//			this.j.punts -= Adapter.adaptarPuntuacion(o.getPunts(),j.factor);
			this.j.punts -= puntos;
		} 
		else if ((Object) o instanceof ObjBonus) {
//			this.j.punts += Adapter.adaptarPuntuacion(o.getPunts(),j.factor);
			this.j.punts += puntos;
		} 
		else if ((Object) o instanceof ObjCaza) {
//			this.j.punts += Adapter.adaptarPuntuacion(o.getPunts(),j.factor);
			this.j.punts += puntos;
		}
		System.out.println(
				"Jugador " + this.j.nom + " ha guanyat " + o.getPunts() + " punts ("+puntos+" punts amb el factor "+this.j.factor+"). Puntuació total: " + this.j.punts);

	}

	public Jugador getJugador() {
		return this.j;
	}

	@Override
	public String toString() {
		return "TipusJugador [j=" + j + "]";
	}

	@Override
	public void actualitzaPunts(ObjecteGuanyat o) {
		// TODO Auto-generated method stub

	}

}
