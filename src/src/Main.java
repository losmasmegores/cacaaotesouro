package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import Adapter.BonusAdapter;
import Adapter.CazaAdapter;
import Adapter.EnemigoAdapter;
import Factory_AbstracFactory.FactoriaObjectes;
import Factory_AbstracFactory.FactoryJugadors;
import Observer.ColectorPunts;
import enums.Colors;
import enums.TEnemic;
import enums.TFactoria;
import enums.TObjBonus;
import enums.TObjCaza;
import enums.TObjMejora;
import enums.TPlayer;
import enums.TVehicles;
import interficies.JugadorDecorator;
import interficies.ObjecteAGuanyar;


public class Main {
	static Connection conn = null;

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); //CREACIO DEL SCANNER
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		
		
		Deque<Jugador> PilaJugadors = new LinkedList<Jugador>(); //GENEREM LA PILA ON ES SITUARAN ELS JUGADORS
		
		
		
//--------------------------------CREACIO DELS JUGADORS--------------------------------
		
		System.out.print("Inserta el numero de jugadors: ");
		int NPlayers = sc.nextInt();
		
		sc.nextLine();
	

		
		ColectorPunts collector = new ColectorPunts(); // CREEM EL ENCARREGAT DE LA GESTIO DE PUNTS
//		
		for (int i = 0; i < NPlayers; i++) {
			System.out.print("Introdueix el teu nom: ");
			String nom = sc.nextLine();
			System.out.print("Selecciona una classe: 1-Comerciant / 2-Guerrer / 3-Pistoler -> ");
			TPlayer classe = SelectTipus(sc.nextInt());
			System.out.println("Selecciona el color:");
			System.out.println("1 - AQUA");
			System.out.println("2 - CYAN");
			System.out.println("3 - LIME");
			System.out.println("4 - YELLOW");
			System.out.println("5 - HOTPINK");
			System.out.println("6 - FUCHSIA");
			System.out.println("7 - RED");
			System.out.println("8 - MEDIUMSPRINGGREEN");
			Colors color = SelectColor(sc.nextInt());
			PilaJugadors.add(FactoryJugadors.getPlayer(classe, nom, 0,color)); //	AFEGIRM EL JUGADOR A LA PILA
			collector.addObserver(PilaJugadors.peekLast()); // AFEGIM EL JUGADOR AL OBSERVADOR
			sc.nextLine();
		}
		Deque<Jugador> players = (Deque<Jugador>) ((LinkedList) PilaJugadors).clone();	
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/cacaaotesouro?" + "user=root&password=super3");
			Statement truncate = conn.createStatement();
			truncate.execute("TRUNCATE TABLE jogo");
			Statement stmt2 = conn.createStatement();
			
			while (!players.isEmpty()){
				Jugador player = players.poll();
				stmt2.execute("INSERT INTO jogo (jugador, puntos) values ('"+player.nom+"',"+0+")"); //AFEGIM ELS USUARIS A LA BD
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		Collections.shuffle((List<?>) PilaJugadors); //  SEMBLA INJUST FER UN DAU CADA COP PER A ESCULLIR QUI LI TOCA, MILLOR FER UN SHUFFLE
//		
//-------------------------------------------------------------------------------------	
//		
//--------------------------CREACIO DELS OBJECTES/ENEMICS------------------------------	

		ArrayList<ArrayList> listas = new ArrayList<ArrayList>();
		ArrayList<Enemics> enemigos = new ArrayList<Enemics>();
		ArrayList<ObjBonus> bonus = new ArrayList<ObjBonus>();
		ArrayList<ObjCaza> caza = new ArrayList<ObjCaza>();
		ArrayList<ObjMejora> mejora = new ArrayList<ObjMejora>();
		ArrayList<Vehicles> vehiculos = new ArrayList<Vehicles>();
		
		enemigos.add((Enemics) FactoriaObjectes.getFactory(TFactoria.Enemics).create(TEnemic.Bestia));
		enemigos.add((Enemics) FactoriaObjectes.getFactory(TFactoria.Enemics).create(TEnemic.Monstre));
		enemigos.add((Enemics) FactoriaObjectes.getFactory(TFactoria.Enemics).create(TEnemic.Pirata));
		
		bonus.add((ObjBonus) FactoriaObjectes.getFactory(TFactoria.ObjBonus).create(TObjBonus.Diamant));
		bonus.add((ObjBonus) FactoriaObjectes.getFactory(TFactoria.ObjBonus).create(TObjBonus.Hamburguesa));
		bonus.add((ObjBonus) FactoriaObjectes.getFactory(TFactoria.ObjBonus).create(TObjBonus.RedBull));
		
		caza.add((ObjCaza) FactoriaObjectes.getFactory(TFactoria.ObjCaza).create(TObjCaza.Cofre));
		caza.add((ObjCaza) FactoriaObjectes.getFactory(TFactoria.ObjCaza).create(TObjCaza.Lingot));
		caza.add((ObjCaza) FactoriaObjectes.getFactory(TFactoria.ObjCaza).create(TObjCaza.Moneda));
		
		mejora.add((ObjMejora) FactoriaObjectes.getFactory(TFactoria.ObjMejora).create(TObjMejora.Espasa));
		mejora.add((ObjMejora) FactoriaObjectes.getFactory(TFactoria.ObjMejora).create(TObjMejora.Laxant));
		mejora.add((ObjMejora) FactoriaObjectes.getFactory(TFactoria.ObjMejora).create(TObjMejora.Pistola));
		
		vehiculos.add((Vehicles) FactoriaObjectes.getFactory(TFactoria.Vehicle).create(TVehicles.Ala_Delta));
		vehiculos.add((Vehicles) FactoriaObjectes.getFactory(TFactoria.Vehicle).create(TVehicles.Barca));
		vehiculos.add((Vehicles) FactoriaObjectes.getFactory(TFactoria.Vehicle).create(TVehicles.Cotxe));
		vehiculos.add((Vehicles) FactoriaObjectes.getFactory(TFactoria.Vehicle).create(TVehicles.Patinet));
		
		listas.add(enemigos);
		listas.add(bonus);
		listas.add(caza);
		listas.add(mejora);
		listas.add(vehiculos);
		
//-------------------------------------------------------------------------------------	
//		JugadorAbst objmillora = null; //1 jugador, decorator compacto
//		JugadorAbst vehicle = null;
		boolean finalitza = false; //
		Random r = new Random();
		System.out.println("COMIENZA EL JUEGO");
		
//-------------------------------------SIMULACIO---------------------------------------	
		while (!finalitza) {
//			System.out.println("WHILE");
			Jugador actual = PilaJugadors.pollFirst(); // SELECCIONEM AL JUGADOR INICIAL
			JugadorDecorator jd = actual; // LI IMPLANTEM EL DECORATOR
			jd = new color(actual.color,jd);
			JugadorAbst objmillora = null; //varios jugador, decorator simple
			JugadorAbst vehicle = null;
			
//			System.out.println(actual);
//			System.out.println(actual.nom);
			int llistaactual = r.nextInt(listas.size());
			
			if(listas.get(llistaactual).get(0) instanceof JugadorAbst) {
				JugadorAbst objactual;
				if(listas.get(llistaactual).get(0).getClass().getSuperclass() == ObjMejora.class) {
					objactual = (ObjMejora) listas.get(llistaactual).get(r.nextInt(listas.get(llistaactual).size()));
					actual.setMejora((ObjMejora)objactual);
//					System.out.println("Jugador "+actual.nom+" ara te un factor de: "+actual.factor);
					objmillora = objactual;
				}
				else {
					objactual = (Vehicles) listas.get(llistaactual).get(r.nextInt(listas.get(llistaactual).size()));
					vehicle = objactual;
				}
//				objactual.setRamen(jd);
				if(objmillora != null) {
					objmillora.setRamen(jd);
					if(vehicle != null) {
						vehicle.setRamen(objmillora);
						System.out.println(vehicle.decorate());
					}
					else System.out.println(objmillora.decorate());
				}
				else {
					vehicle.setRamen(jd);
					System.out.println(vehicle.decorate());
				}
//				System.out.println(objactual.decorate());
				
			}
			else {
				ObjecteAGuanyar objactual = null;
				if(listas.get(llistaactual).get(0).getClass().getSuperclass() == Enemics.class) {
					objactual = (Enemics) listas.get(llistaactual).get(r.nextInt(listas.get(llistaactual).size()));
					collector.setAdapter(new EnemigoAdapter());
//					System.out.println(objactual);
				}
				else if(listas.get(llistaactual).get(0).getClass().getSuperclass() == ObjBonus.class) {
					objactual = (ObjBonus) listas.get(llistaactual).get(r.nextInt(listas.get(llistaactual).size()));
					collector.setAdapter(new BonusAdapter());
//					System.out.println(objactual);
				}
				else if(listas.get(llistaactual).get(0).getClass().getSuperclass() == ObjCaza.class) {
					objactual = (ObjCaza) listas.get(llistaactual).get(r.nextInt(listas.get(llistaactual).size()));
					collector.setAdapter(new CazaAdapter());
//					System.out.println(objactual);
				}
				collector.setPunts(new ObjecteGuanyat(actual,objactual));
			}
			CambioPuntaçao(actual, actual.punts);
//			
//			
			PilaJugadors.addLast(actual); // EL TORNEM A COLOCAR AL FINAL DE LA PILA
			if(actual.punts > 570 || actual.punts < -570) {
				finalitza = finalizar();
				System.out.println("HA GANADO: "+actual);
			}
		}
//-------------------------------------------------------------------------------------
		
		
		while (!PilaJugadors.isEmpty()) { // ELIMINEM ELS JUGADORS DEL OBSERVER
			Jugador actual = PilaJugadors.pollFirst();
//			System.out.println(actual);
			CambioPuntaçao(actual, actual.punts);
			collector.removeObserver(actual);
		}
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/cacaaotesouro?" + "user=root&password=super3");			
			
			
			Statement stmt3 = conn.createStatement();
			Statement delete = conn.createStatement();
		
			
			if(stmt3.execute("SELECT * FROM jogo WHERE puntos < 500"));
			{
				rs2 = stmt3.getResultSet();
				while(rs2.next())
				{
					System.out.println("ESTE "+rs2.getString("jugador")+" TIENE "+rs2.getString("puntos")+" SOLO ASIQUE QUEDA ELIMINADO");
					delete.execute("DELETE FROM jogo WHERE jugador = '"+rs2.getString("jugador")+"'");
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		} 
		
		
//-------------------------------PROVA DECORATOR---------------------------------------
		//JugadorDecorator jd = new ObjMejora(new Vehicles(new color(new Jugador())));
//				JugadorDecorator jj = new Jugador("Pepe");
//				JugadorDecorator jj1 = new ObjMejora(jj);
//				JugadorDecorator jj2 = new color(Colors.AQUA,jj1);
//				JugadorDecorator jj3 = new Vehicles(jj2);
//				
//				System.out.println(jj3.decorate());
//				JugadorDecorator j = new Jugador();
//				JugadorDecorator j1 = new ObjMejora(j);
//				JugadorDecorator j2 = new color(j1);
//				JugadorDecorator j3 = new Vehicles(j2);
		

		
//				System.out.println(j3.decorate());
		
		
//				ObjMejora a = (ObjMejora)FactoriaObjectes.getFactory(TFactoria.ObjMejora).create(TObjMejora.Espasa, j);
//				Vehicles a2 = (Vehicles) FactoriaObjectes.getFactory(TFactoria.Vehicle).create(TVehicles.Ala_Delta, a);
//				JugadorDecorator j2 = new color(Colors.HOTPINK,a2);
//				System.out.println(j2.decorate());
		
//-------------------------------------------------------------------------------------

//---------------------------------PROVA JDBC------------------------------------------
//		Jugador j1 = new Jugador("Juan");
//		Jugador j2 = new Jugador("David");
//		Jugador j3 = new Jugador("Raul");
//		Jugador j4 = new Jugador("Alex");
//		
//		ArrayList<Jugador> lista = new ArrayList<Jugador>();
//		
//		lista.add(j1);
//		lista.add(j2);
//		lista.add(j3);
//		lista.add(j4);
//		
									//EJERCICIO 6 CON COMPARATOR Y COMPARABLE
									//EJERCICIO 6 CON COMPARATOR Y COMPARABLE
									//EJERCICIO 6 CON COMPARATOR Y COMPARABLE
//		j1.punts = 10;
//		j2.punts = 40;
//		j3.punts = 20;
//		j4.punts = 50;
//		
//		//EJERCICIO 6 COMPARATOR Y COMPARABLE
//		System.out.println("LISTA PRE-SORT");
//		System.out.println(lista);
//		
//		Collections.sort(lista);
//
//		System.out.println();
//		System.out.println("LISTA POST-SORT (ALFABETICAMENTE)");
//		System.out.println(lista);
//		
//		lista.sort(new comparatorNom());
//		System.out.println();
//		System.out.println("LISTA CON COMPARATOR PUNTOS DESCENDENTE");
//		System.out.println(lista);
		
									//EJERCICIO 6 CON BASE DE DATOS
									//EJERCICIO 6 CON BASE DE DATOS
									//EJERCICIO 6 CON BASE DE DATOS
//		try {
//			conn = DriverManager.getConnection("jdbc:mysql://localhost/cacaaotesouro?" + "user=root&password=super3");			
//			
//			Statement truncate = conn.createStatement();
//			truncate.execute("TRUNCATE TABLE jogo");
//			Statement stmt2 = conn.createStatement();
//			
//			for (int i = 0; i < lista.size(); i++) {				
//				stmt2.execute("INSERT INTO jogo (jugador, puntos) values ('"+lista.get(i).nom+"',"+lista.get(i).punts+")");
//			}
//			
//			CambioPuntaçao(j1, 421432);
//			CambioPuntaçao(j2, 600);
//			CambioPuntaçao(j3, -15);
//			CambioPuntaçao(j4, 20000);
//			Statement stmt3 = conn.createStatement();
//			Statement delete = conn.createStatement();
//		
//			
//			if(stmt3.execute("SELECT * FROM jogo WHERE puntos < 500"));
//			{
//				rs2 = stmt3.getResultSet();
//				while(rs2.next())
//				{
//					System.out.println("ESTE "+rs2.getString("jugador")+" TIENE "+rs2.getString("puntos")+" SOLO ASIQUE QUEDA ELIMINADO");
//					delete.execute("DELETE FROM jogo WHERE jugador = '"+rs2.getString("jugador")+"'");
//				}
//			}
//
//		} catch (Exception e) {
//			// TODO: handle exception
//		} 
		
//		if(actual.nom.equals("a1"))actual.setMejora((ObjMejora)FactoriaObjectes.getFactory(TFactoria.ObjMejora).create(TObjMejora.Espasa, actual));
//		System.out.println(actual.factor);
//		if(actual.nom.equals("a1"))collector.setPunts(new ObjecteGuanyat(actual, (Enemics) FactoriaObjectes.getFactory(TFactoria.Enemics).create(TEnemic.Pirata)));
//		else if(actual.nom.equals("a2"))collector.setPunts(new ObjecteGuanyat(actual, (ObjCaza) FactoriaObjectes.getFactory(TFactoria.ObjCaza).create(TObjCaza.Lingot)));
//		else collector.setPunts(new ObjecteGuanyat(actual, (ObjBonus) FactoriaObjectes.getFactory(TFactoria.ObjBonus).create(TObjBonus.Hamburguesa)));
		
//-------------------------------------------------------------------------------------

		sc.close(); // TANQUEM EL SCANNER
	}
		
	
	/**
	 * Retorna true si ...
	 * en cas contrari, es continua amb la partida.
	 * 
	 */
	public static boolean finalizar() {
		return true;
	}
	
	/**
	 * Transforma un int en el tipus de jugador seleccionat
	 * 
	 * @param num Classe seleccionada
	 * @return Tipus de jugador en el {@code enum} TPlayer
	 */
	public static TPlayer SelectTipus(int num) {
		switch (num) {
		case 1:
			return TPlayer.Comerciant;
		case 2:
			return TPlayer.Guerrer;
		case 3:
			return TPlayer.Pistoler;

		default:
			return null;
		}
	}	
	
	public static Colors SelectColor(int num) {
		switch (num) {
		case 1:
			return Colors.AQUA;
		case 2:
			return Colors.CYAN;
		case 3:
			return Colors.LIME;
		case 4:
			return Colors.YELLOW;
		case 5:
			return Colors.HOTPINK;
		case 6:
			return Colors.FUCHSIA;
		case 7:
			return Colors.RED;
		case 8:
			return Colors.MEDIUMSPRINGGREEN;

		default:
			return null;
		}
	}	
	
	public static void informaçao(Jugador j) {
		Statement stmt3 = null;
		ResultSet rs2 = null;
		
		try {
			
			 conn = DriverManager.getConnection("jdbc:mysql://localhost/cacaaotesouro?" + "user=root&password=super3");			

			 stmt3 = conn.createStatement();
			 rs2 = null;
			if(stmt3.execute("SELECT * FROM jogo WHERE jugador = '"+j.nom+"'"));
			{
				rs2 = stmt3.getResultSet();
				while(rs2.next())
				{
					System.out.println("JUGADOR "+rs2.getString("jugador")+" TIENE "+rs2.getString("puntos")+" PUNTOS");
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public static void CambioPuntaçao(Jugador j, double punts) {
		
		Statement stmt3 = null;
		ResultSet rs2 = null;
		
		try {		
			 conn = DriverManager.getConnection("jdbc:mysql://localhost/cacaaotesouro?" + "user=root&password=super3");			
			 stmt3 = conn.createStatement();
			 stmt3.execute("UPDATE jogo set puntos = "+punts+" where jugador = '"+j.nom+"'");		
			 j.punts = punts;
			 informaçao(j);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
}
