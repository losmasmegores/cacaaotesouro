package src;

import interficies.JugadorDecorator;

public abstract class JugadorAbst implements JugadorDecorator{ // interfaz
	
	private JugadorDecorator ramen;
	
	public JugadorAbst() {
		this.ramen = null;
	}
	
	public JugadorAbst(JugadorDecorator object)
	{
		this.ramen = object;
	}

	public JugadorDecorator getRamen() {
		return ramen;
	}

	public void setRamen(JugadorDecorator ramen) {
		this.ramen = ramen;
	}

	@Override
	public String decorate() {
		// TODO Auto-generated method stub
		return ramen.decorate();
	}
}
