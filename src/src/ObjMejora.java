package src;

import enums.TObjMejora;
import interficies.JugadorDecorator;

public class ObjMejora extends JugadorAbst{
	String nom;
	TObjMejora tipus;
	double factor;
	
	public ObjMejora(JugadorDecorator jd){
		super(jd);
	}
	
	public ObjMejora(TObjMejora tipus, double punts,JugadorDecorator jd) {
		super(jd);
		this.nom = tipus.toString();
		this.tipus = tipus;
		this.factor = punts;
	}
	public ObjMejora(String nom, TObjMejora tipus, double punts, JugadorDecorator jd) {
		super(jd);
		this.nom = nom;
		this.tipus = tipus;
		this.factor = punts;
	}
	public ObjMejora(TObjMejora tipus, double punts) {
		this.nom = tipus.toString();
		this.tipus = tipus;
		this.factor = punts;
	}

	@Override
	public String toString() {
		return "ObjMejora [nom=" + nom + ", tipus=" + tipus + ", factor=" + factor + "]";
	}

	public void setJugadorDecorator(JugadorDecorator jd) {
		this.setRamen(jd);
	}
	public String decorate()
	{
		return super.decorate() + " tiene un "+this.nom;
	}
}
