package src;

import enums.Colors;
import enums.TPlayer;
import interficies.JugadorDecorator;

public class Jugador implements JugadorDecorator, Comparable<Jugador>{ // interfaz
	
	public String nom;
	public double punts;
	public int saldo;
	public TPlayer Clase;
	public double factor;
	public Colors color;
	
	//----------------------------------CONSTRUCTORS--------------------------------------

	public Jugador() {
		this.nom = null;
		this.punts = 0;
		this.saldo = 0;
		this.Clase = null;
		this.factor = 0;
		this.color = Colors.RED;
	}
	public Jugador(String nom) {
		this.nom = nom;
		this.punts = 0;
		this.saldo = 0;
		this.Clase = null;
		this.factor = 0;
		this.color = Colors.RED;
	}
	public Jugador(TPlayer tipus,String nom) {
		this.nom = nom;
		this.punts = 0;
		this.saldo = 0;
		this.Clase = tipus;
		this.factor = 0;
		this.color = Colors.RED;
	}
	
	public Jugador(TPlayer tipus, String nom, int saldo) {
		this.nom = nom;
		this.punts = 0;
		this.saldo = saldo;
		this.Clase = tipus;
		this.factor = 0;
		this.color = Colors.RED;
	}
	public Jugador(TPlayer tipus, String nom, int saldo, Colors color) {
		this.nom = nom;
		this.punts = 0;
		this.saldo = saldo;
		this.Clase = tipus;
		this.factor = 0;
		this.color = color;
	}
	
	public Jugador(TPlayer tipus,String nom, int punts, int saldo) {
		this.nom = nom;
		this.punts = punts;
		this.saldo = saldo;
		this.Clase = tipus;
		this.factor = 0;
		this.color = Colors.RED;
	}

	
	@Override
	public String toString() {
		return "Jugador [nom=" + nom + ", punts=" + punts + ", saldo=" + saldo + ", classe= "+ Clase +"]";
	}
	@Override
	public String decorate() {
		// TODO Auto-generated method stub
		return "El Jugador "+nom;
	}
	@Override
	public int compareTo(Jugador o) {
		
		return this.nom.compareTo(o.nom);
	}
	
	public void setMejora(ObjMejora obj) {
		this.factor = obj.factor;
	}
}
