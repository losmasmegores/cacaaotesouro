package src;

import enums.TEnemic;
import interficies.ObjecteAGuanyar;

public class Enemics implements ObjecteAGuanyar{
	String nom;
	TEnemic tipus;
	int punts_a_restar;
	
	public Enemics(String nom, TEnemic tipus, int punts) {
		this.nom = nom;
		this.tipus = tipus;
		this.punts_a_restar = punts;
	}
	public Enemics(TEnemic tipus, int punts) {
		this.nom = tipus.toString();
		this.tipus = tipus;
		this.punts_a_restar = punts;
	}
	@Override
	public String toString() {
		return "Enemics [nom=" + nom + ", tipus=" + tipus + ", punts_a_restar=" + punts_a_restar + "]";
	}
	@Override
	public int getpunts() {
		// TODO Auto-generated method stub
		return 0-this.punts_a_restar;
	}
	
	
}
