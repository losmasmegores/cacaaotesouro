package src;

import enums.TVehicles;
import interficies.JugadorDecorator;

public class Vehicles extends JugadorAbst{
	String nom;
	TVehicles tipus;
	
	
	public Vehicles(JugadorDecorator jd) {
		super(jd);
	}
	
	public Vehicles(TVehicles tipus, JugadorDecorator jd) {
		super(jd);
		this.nom = tipus.toString();
		this.tipus = tipus;
	}
	public Vehicles(String nom, TVehicles tipus, JugadorDecorator jd) {
		super(jd);
		this.nom = nom;
		this.tipus = tipus;
	}
	
	@Override
	public String toString() {
		return "Vehicles [nom=" + nom + ", tipus=" + tipus + "]";
	}

	public Vehicles(TVehicles tipus) {
		this.nom = tipus.toString();
		this.tipus = tipus;
	}
	
	public void setJugadorDecorator(JugadorDecorator jd) {
		this.setRamen(jd);
	}
	public String decorate()
	{
		return super.decorate() + " tiene un "+this.nom;
	}

}
