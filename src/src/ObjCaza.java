package src;

import enums.TObjCaza;
import interficies.ObjecteAGuanyar;

public class ObjCaza implements ObjecteAGuanyar{
	String nom;
	TObjCaza tipus;
	int punts_a_sumar;
	
	public ObjCaza(TObjCaza tipus, int punts) {
		this.nom = tipus.toString();
		this.tipus = tipus;
		this.punts_a_sumar = punts;
	}
	public ObjCaza(String nom, TObjCaza tipus, int punts) {
		this.nom = nom;
		this.tipus = tipus;
		this.punts_a_sumar = punts;
	}
	
	@Override
	public String toString() {
		return "ObjCaza [nom=" + nom + ", tipus=" + tipus + ", punts_a_sumar=" + punts_a_sumar + "]";
	}
	@Override
	public int getpunts() {
		// TODO Auto-generated method stub
		return 0+this.punts_a_sumar;
	}
}
