package src;

import interficies.ObjecteAGuanyar;

public class ObjecteGuanyat {
	Jugador recogedor;
	ObjecteAGuanyar premio;
	double puntos;
	
	public ObjecteGuanyat(Jugador j, ObjecteAGuanyar o) {
		this.recogedor = j;
		this.premio = o;
		this.puntos = o.getpunts();	
	}
	public Jugador getJugador() {
		return this.recogedor;
	}
	public double getPunts() {
		return this.puntos;
	}
}
