package Observer;

import java.util.ArrayList;
import java.util.List;

import Adapter.PuntuacionAdapter;
import src.Jugador;
import src.ObjecteGuanyat;
import src.TipusJugador;

public class ColectorPunts implements Observable{
    private List<TipusJugador> observadors = new ArrayList<>();
    private double puntuacio = 0;
    PuntuacionAdapter adapter;

    public ColectorPunts(PuntuacionAdapter a) {
    	this.adapter = a;
    }
    public ColectorPunts() {
    	this.adapter = null;
    }
    public void addObserver(Jugador observador) {
        observadors.add(new TipusJugador(observador));
    }

    public void removeObserver(Jugador observador) {
    	for (int i = 0; i < this.observadors.size(); i++) {
			TipusJugador tipusJugador = this.observadors.get(i);
			if(tipusJugador.getJugador().equals(observador)) {
				observadors.remove(tipusJugador);
			}
		}
        
    }

    public void setAdapter(PuntuacionAdapter e) {
    	this.adapter = e;
    }
    public void setPunts(ObjecteGuanyat o) {
        puntuacio = o.getPunts();
        notificarObservadors(o);
    }

    public void notificarObservadors(ObjecteGuanyat o) {
        for (TipusJugador observador : observadors) {
        	if(observador.getJugador().equals(o.getJugador())) observador.actualitzaPunts(o,this.adapter);
        }
    }

}
