package Observer;

import src.Jugador;
import src.ObjecteGuanyat;

public interface Observable {
	void addObserver(Jugador observer);
    void removeObserver(Jugador observer);
    void notificarObservadors(ObjecteGuanyat objeto);
}
