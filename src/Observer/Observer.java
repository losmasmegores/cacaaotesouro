package Observer;

import Adapter.PuntuacionAdapter;
import src.ObjecteGuanyat;

public interface Observer {
	void actualitzaPunts(ObjecteGuanyat o);

	void actualitzaPunts(ObjecteGuanyat o, PuntuacionAdapter Adapter);
}
