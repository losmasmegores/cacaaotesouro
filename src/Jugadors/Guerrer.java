package Jugadors;

import enums.Colors;
import enums.TPlayer;
import src.Jugador;

public class Guerrer extends Jugador{
	String nom;
	int punts;
	int saldo;
	TPlayer Clase;
	
	//----------------------------------CONSTRUCTORS--------------------------------------

		public Guerrer(String nom) {
			super(TPlayer.Guerrer,nom);
		}
		
		public Guerrer(String nom, int saldo) {
			super(TPlayer.Guerrer,nom,saldo);
		}
		public Guerrer(String nom, int saldo, Colors color) {
			super(TPlayer.Guerrer,nom,saldo,color);
		}
		public Guerrer(String nom, int punts, int saldo) {
			super(TPlayer.Guerrer,nom,punts,saldo);
		}
		
		
		//------------------------------------------------------------------------------------


	
	
}
