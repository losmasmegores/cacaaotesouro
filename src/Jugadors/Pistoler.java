package Jugadors;

import enums.Colors;
import enums.TPlayer;
import src.Jugador;

public class Pistoler extends Jugador{
	String nom;
	int punts;
	int saldo;
	TPlayer Clase;
	
	//----------------------------------CONSTRUCTORS--------------------------------------

	public Pistoler(String nom) {
		super(TPlayer.Pistoler,nom);
	}
	
	public Pistoler(String nom, int saldo) {
		super(TPlayer.Pistoler,nom,saldo);
	}
	public Pistoler(String nom, int saldo, Colors color) {
		super(TPlayer.Pistoler,nom,saldo,color);
	}
	public Pistoler(String nom, int punts, int saldo) {
		super(TPlayer.Pistoler,nom,punts,saldo);
	}
	
	
	//------------------------------------------------------------------------------------



	
	
}
