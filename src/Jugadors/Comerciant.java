package Jugadors;

import enums.Colors;
import enums.TPlayer;
import src.Jugador;

public class Comerciant extends Jugador{
	
	//----------------------------------CONSTRUCTORS--------------------------------------

	public Comerciant(String nom) {
		super(TPlayer.Comerciant,nom);
	}
	
	public Comerciant(String nom, int saldo) {
		super(TPlayer.Comerciant,nom,saldo);
	}
	public Comerciant(String nom, int saldo,Colors color) {
		super(TPlayer.Comerciant,nom,saldo,color);
	}
	public Comerciant(String nom, int punts, int saldo) {
		super(TPlayer.Comerciant,nom,punts,saldo);
	}
	
	
	//------------------------------------------------------------------------------------
	
}
