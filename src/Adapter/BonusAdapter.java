package Adapter;

public class BonusAdapter implements PuntuacionAdapter {
//	ObjBonus oBonus;

	@Override
	public double adaptarPuntuacion(double puntuacion) {
		return 0;
	}

	@Override
	public double adaptarPuntuacion(double puntuacion, double factor) {
		double factores = factor;
		if (factores != 0) {
			puntuacion = puntuacion / factores;
		}
		return puntuacion;

	}

}
