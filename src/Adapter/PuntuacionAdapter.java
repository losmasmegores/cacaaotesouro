package Adapter;

public interface PuntuacionAdapter {
	double adaptarPuntuacion(double puntuacion);

	double adaptarPuntuacion(double puntuacion, double factor);
}
