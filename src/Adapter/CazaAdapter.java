package Adapter;

import interficies.ObjecteAGuanyar;
import src.ObjCaza;

public class CazaAdapter implements PuntuacionAdapter {
	
//  HAY QUE PILLAR EL FACTOR DE AQUI
//	ObjCaza oCaza;

	@Override
	public double adaptarPuntuacion(double puntuacion) {
		return 0;
	}

	@Override
	public double adaptarPuntuacion(double puntuacion, double e) {
		double factor = e;
		if (factor != 0) {
			puntuacion = puntuacion / factor;
		}
		return puntuacion;
	}

}